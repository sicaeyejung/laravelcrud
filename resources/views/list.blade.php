@extends('layouts.master')
@section('title','List')
@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
@endsection
@section('content')
@if (Session::has('message'))
<div class="alert alert-success">
    {{Session::get('message')}}
</div>
@endif
<a href="{{url('people/create')}}">
    <button type="button" class="btn btn-success">Create</button>
</a>
    <table class="table table-dark">
        <thead>
            <th>ID</th>
            <th>Fname</th>
            <th>lname</th>
            <th>Age</th>
            <th>Actions</th>
        </thead>
        <tbody>
            @foreach ($people as $p)
            <tr>
                <td>{{$p->id}}</td>
                <td>{{$p->fname}}</td>
                <td>{{$p->lname}}</td>
                <td>{{$p->age}}</td>
                {{-- <td>{{$p->Create_date}}</td> --}}
                <td>
                   
                <a href="{{url ('people/' . $p->id .'/edit')}}">
                    <button type="submit" class="btn btn-success">Edit</button>
                </a>
                <form action="{{ url('people/'. $p->id)}}" method="POST" >
                 @csrf 
                 @method('delete')
                 <button type="submit" class="btn btn-danger">Delete</button>
                </form>
                </td>
            </tr>
            @endforeach
        

    </table>
@endsection
